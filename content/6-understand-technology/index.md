# 6. Understand Your Technology

![](images/7.jpg)

## STEP 6

---

To run an effective social-first news agency, you need a solid understanding of your technology and available connectivity. Your staff should have a mobile connection whenever possible, the lowest bandwidth social story still needs a voice or SMS connection. If data connectivity is too weak to publish multimedia immediately, take this as an opportunity and not a limitation.

Engage your audience in the process, provide them updates as you have them. Smart devices, and digital cameras are capable of recording all elements of multimedia in one device. When connectivity permits, you’ll be able to publish content as soon as you have it.

The production power of smart devices is the key promise of mobile technology. They offer not just connectivity, but multimedia production power in a small package. Smart devices enable your team to create, assemble, and publish from the field. You may not want your staff to publish complete features without editorial oversight, but the ability to publish the story over time will engage your audience and increase your value. Release initial clips as soon as possible, updating as the story evolves and further contextualizing the story as more information becomes available.

Multimedia is not an ends in itself. Social stories are crafted to have a narrative. The manner of publishing and releasing the material should complement the story. A brief tweet or text-only status update sets the stage and lets the audience know what news is coming. Publishing a single photo or video clip with a caption ahead of a more in-depth story piques interest and intrigues the audience. As you report your story, think through how this narrative will develop. It will help you present better material and avoid producing content you will never use.

![](images/8.jpg)

---
