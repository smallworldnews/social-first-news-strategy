# 1. Establish a Clear Editorial Proposition

![](images/2.jpg)

### STEP 1

---

Every great news strategy is built on engaging the public and building an audience by offering something inspiring, something they can’t get anywhere else. A social-first news strategy leverages evolving platforms and technology to maximize your reach and engage your audience as primary and secondary sources.

What kind of stories will your organization tell? This is the crux of your editorial proposition. Before you can determine the size of your target audience, you must decide what stories you want to tell. Will your organization focus on a specific topic niche, such as politics, economics, or sports? Or, will you produce a more general product, covering a broad overview of daily or weekly events? You may decide to take a general approach to stories, but focus on appealing to a specific demographic.

---

You want to create social-first stories. Engagement should be a core feature of storytelling and reporting. Engaging your audience and leverage new social platforms should define your editorial strategy. You want to craft your voice and your individual social strategy. In order to ensure you are leading the strategy, and not chasing after it, you must establish an editorial focus. To do this, make a clear decision about your intended audience.

* The Audience should provide you content.
* Stories must move the audience to inspire action.
* Initial Stories entice and engage the audience, to prepare them to wait for upcoming longer features.


In a social environment you often begin your coverage as a simple headline and short dispatch as news breaks. Push it out quickly on social platforms such as Facebook, Twitter, YouTube and others that have traction with your audience. Invite the audience to contribute while you develop your own reporting. Combine staff reporting with primary and secondary source material provided by your audience.

---
