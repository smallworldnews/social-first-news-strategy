# 2. Determine your Target Audience

![](images/3.jpg)

## STEP 2

---

Understanding your audience is essential to defining a successful editorial focus. Whether the audience is national, international, or regional can dramatically change your focus. Include your audience when researching / planning the story calendar. Reach out to your audience via Facebook and Twitter can provide essential insight and improve coverage, as well as increasing your total audience.

The interests of your audience should drive your strategy. If your publication is focused on general news, you have a broader potential audience but must also take a more general approach to content production and news gathering. If your publication is primarily about a specific ethnic group or a narrow topic, such as a specific type of agriculture or business, your strategy is easier to define, but it may be more difficult to reach sustainability.

---

The latest demographics reveal the rapidly increasing potential of social-first news strategies in the Middle East and North Africa. As of 2013, more than 125 million citizens in MENA access the Internet and 53 million actively participate on social media. 80 percent of Internet users in MENA spend more than an hour updating social networks. This indicates a population with a high likelihood of being receptive to a social-first approach.

* Converse and engage on Twitter, don't simply broadcast.
* Direct the conversation by creating clear hashtags but be flexible. Adapt others' hashtags if they are trending higher.
* A photo plus a caption is doable by anyone with a mobile phone.
* Ask questions, poll your audience, experts are everywhere.

An easy way to begin a conversation with your audience by attaching a hashtag to your question, title, or status update. In 2009, in an effort to coordinate reporting on the Presidential election via Twitter, Small World News’ co-founder Brian Conley ([@BaghdadBrian](https://twitter.com/BaghdadBrian)) made a concerted effort to push the [#afghan09](https://twitter.com/search?q=afghan09) tag.

According to the WebEcology research project:

> “Out of the 483 different hashtags used at least three times, #Afghan09 was most prominent, first adopted by [@aliveinafghan](https://twitter.com/aliveinafghan), [@smallworldnews](https://twitter.com/SmallWorldNews), and [@BaghdadBrian](https://twitter.com/BaghdadBrian). #Afghanistan (a non-unique hashtag) had the next highest frequency, followed by #AfghanElection, originally adopted by [@pajhwok](https://twitter.com/pajhwok).”

---
