# 8. Understand the Additional Challenges to Your Success

![](images/10.jpg)

## STEP 8

---

Be sure you know the regulatory landscape and the laws governing journalists and the media in the country or region you work. Just because laws to protect journalists exist does not mean that government, local leadership and security forces will obey the law, but understanding the law can be a first step toward improving it.

Assess the telecommunications infrastructure your organization will depend on. Contracting with the wrong mobile or internet service provider will put a serious restriction on your potential for success. Understanding the limitations of available providers can help you plan your publishing cycle. It is also important to understand the connectivity available to your audience. If your reporters rely on high-speed connections not available to much of your audience, it’s important to ensure they don’t produce content equally inaccessible.

Often in emerging media environments or repressive states, the media law is not well established or changes frequently. It is important to be aware of these issues and work to protect your staff when possible.

Ensure your staff adhere to professional codes of conduct. Even in countries where media laws are very restrictive, observing the principles outlined may help you avoid accusations of libel or treason.

* What does the competition look like?
* Who else serves your audience?
* What are you offering that is different and will attract viewers?

Whether your audience is national, international, or regional will make a big difference. If your organization intends to generate revenue by reselling content to news buyers, this must be represented in your planning.

Include your intended audience when researching and planning your story calendar. Outreach to the audience via Facebook and Twitter can provide essential insight and improve coverage as well as increasing the total audience.

Stories must have broad appeal to consumers and differentiate the agency from content offered by competitors. Without this you will be unable to compete for limited ad dollars and sponsors. Engage the audience and har- ness their collective power to research and report the news. This will grow your audience and deepen your understanding of individual audience groups, improving your value to advertisers.

Understand who your audience is and what they want. If your publication is focused on general news, you have a broader potential audience but must also take a more general approach to content production and news gathering.

If your publication is primarily about a specific ethnic group or a narrow topic, such as a specific type of agriculture or business, your strategy is easier to define, but it may be more difficult to reach sustainability. What are the additional challenges to your success?

---
