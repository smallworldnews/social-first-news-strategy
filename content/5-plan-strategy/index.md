# 5. Plan Your Social-First Strategy

![](images/6.jpg)

## STEP 5

---

A smart strategy for daily coverage is as important as staffing. Every day should start with an editorial meeting. The Editor-in-Chief leads story meetings to define your organization’s content priorities. It is then up to the social editor and reporters to take these priorities to the social web, research via Twitter, Facebook, etc. the reporter's primary job is to fact-check rumors and updates from the social web, creating accurate, fact-based updates.

The meeting should be quick and cover the stories of the day. Sometimes a bit of pre-planning for upcoming events should be included. Daily meetings and a clear coverage plan are essential to a clear staff focus on the news as it happens. Daily meeting are also essential to identifying and following the most interesting stories as they develop into in-depth features.

---

![](images/coverage-workflow.png)

---

Not all stories are planned. Some news breaks during the day. Consider a multi-step publication process:

1. When news breaks, staff must first send a text message or call their editor.
2. Once approved, publish an initial headline-style update.
3. Follow breaking news and headlines with more detailed text stories and a photo or short video clip as soon as possible.
4. Add more carefully edited videos, additional photos and follow-up coverage as new information -- and connectivity -- permit.
5. Once you've provided breaking coverage, you can delay adding additional information until you are at a location with good connectivity.

*For more on this topic, see the ‘Planning your Coverage’ course.*

---
