# 9. Make Social-First News

![first image](images/11.jpg)

## STEP 9

---

If you’ve read through all the steps above, you’re ready to get started. Gather your staff together, consider each of the steps and develop a roadmap for implementation. You may not adapt every step outlined, but each step has something to offer. Each step is also an ingredient necessary for most organizations to craft a successful Social-First Strategy.

Review this list of the elements present in a Social-First strategy, and check that your plan provides each of them the proper weight needed by your specific organization.  

* Editorial Focus
* Target Audience
* Social-First Stories
* Social-First Staffing
* Social-First Editorial Plan
* Familiarity with Technology
* Competitive Marketing Plan
* Understand Additional Challenges

It is up to you to decide how to implement each element of your plan. Tell great social-first stories and your organization’s future will be bright. Increasing numbers of news consumers are getting their news from mobile, more than 75% of mobile news consumers access the news several times per day. In 2013, Reuters’ Digital News Report found those who use smartphones and tablets are more likely to go straight to a known news brand. You need to become that known brand for your target audience.

The social-first news strategy will enable you to tell stories faster and with greater depth. Enlisting your audience to not only fact-check, but act as primary and secondary sources will encourage loyalty and word-of-mouth marketing. Publishing your content in social-first formats will place you where the audience already is, keeping you at the front of their mind each time they return to social media. News consumers between 18-34 all prefer social media as a gateway to news consumption. This demographic is leading the trend. Follow the social-first strategy to get your news organization out ahead of the curve.

---
